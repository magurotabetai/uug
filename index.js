import 'normalize.css';
import { h, app } from 'hyperapp';

const width = 300;
const height = 300;
const FILTERS = [
  'none',
  '_1977',
  'aden',
  'amaro',
  'brannan',
  'brooklyn',
  'clarendon',
  'gingham',
  'hudson',
  'inkwell',
  'kelvin',
  'lark',
  'lofi',
  'mayfair',
  'moon',
  'nashville',
  'perpetua',
  'reyes',
  'rise',
  'slumber',
  'stinson',
  'toaster',
  'valencia',
  'walden',
  'willow',
  'xpro2',
];

const state = {
  size: 100,
  // SMALL NORMAL BIG
  guruguru: false,
  rainbow: false,
  sin: false,
  context: null,
  filter: 'none',
};

const renderShit = (context, size) => {
  const newSize = `${size}px`;
  const fontArgs = context.font.split(' ');
  context.font = newSize + ' ' + fontArgs[fontArgs.length - 1];

  const x = width / 2 - size / 2;
  const y = width / 2 + size / 2;
  context.strokeText('💩', x, y);
};

let ang = 0;
let sx = 0.0;

const actions = {
  initCanvas: canvas => ({ size }) => {
    const context = canvas.getContext('2d');

    renderShit(context, size);

    return { context };
  },
  rotate: () => state => {
    const { context, sin, guruguru } = state;

    if (!guruguru) return null;
    if (sin) sx += 0.2;

    const size = sin ? parseInt(Math.sin(sx) * 50 + 100, 10) : state.size;

    context.clearRect(0, 0, width, height);
    const newSize = `${size}px`;
    const fontArgs = context.font.split(' ');
    context.font = newSize + ' ' + fontArgs[fontArgs.length - 1];

    const x = width / 2 - size / 2 + size / 2;
    const y = width / 2 + size / 2;

    context.translate(width / 2, height / 2);

    context.rotate((Math.PI / 180) * (ang += 1));
    context.translate(-width / 2, -height / 2);

    context.strokeText('💩', x, y);

    if (sin) return { size };
    return null;
  },
  toggleGuruguru: () => ({ guruguru }) => ({ guruguru: !guruguru }),
  toggleRainbow: () => ({ rainbow }) => ({ rainbow: !rainbow }),
  toggleSin: () => ({ sin }) => ({ sin: !sin }),
  updateFilter: filter => ({ filter }),
  changeSize: value => ({ context }) => {
    if (context === null) return null;
    const size = parseInt(value, 10);

    return { size };
  },
};

const view = (state, actions) => (
  <div>
    <h1>アルティメットウンコジェネレーター</h1>
    <canvas
      oncreate={actions.initCanvas}
      width={width}
      height={height}
      class={state.filter === 'none' ? '' : state.filter}
    />
    <br />
    <label>
      サイズ: {state.size}px
      <input
        type="range"
        value={state.size}
        min="0"
        max="200"
        oninput={e => actions.changeSize(e.target.value)}
        // data-unit="%"
      />
    </label>

    <label>
      <input
        type="checkbox"
        name="guruguru"
        onchange={actions.toggleGuruguru}
        checked={state.guruguru}
      />
      グルグル
    </label>
    <label>
      <input type="checkbox" name="sin" onchange={actions.toggleSin} checked={state.sin} />
      size = x => Math.sin(x) * 50 + 50
    </label>
    <br />
    <label>
      インスタグラムのフィルター
      <select name="filter" id="filter" onchange={e => actions.updateFilter(e.target.value)}>
        {FILTERS.map(filter => (
          <option value={filter}>{filter}</option>
        ))}
      </select>
    </label>
  </div>
);

const main = app(state, actions, view, document.querySelector('#app'));

setInterval(main.rotate, 20);
